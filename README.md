# n32g457_esp8266_smart_home

#### 介绍
本项目打造了一个可私有化部署的智能家用式告警系统，支持告警系统推送至微信客户端。

#### 软件架构

整个系统的核心架构图如下所示：

![基于N32G457和RT-Thread全新打造的私有化定制家用式智能告警系统-系统架构图](https://s2.loli.net/2022/03/24/68vsl1SckXLJ5fG.png)

整个应用软件部分的程序框架如下图所示：

![image-20220325002533828](https://s2.loli.net/2022/03/25/M4WxAgdzpRoyv78.png)

#### 安装教程

请参考[项目介绍文档](https://club.rt-thread.org/ask/article/3564.html)进行。

#### 使用说明

请参考[项目介绍文档](https://club.rt-thread.org/ask/article/3564.html)进行。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特别说明

暂无。
