################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../applications/key.c \
../applications/led.c \
../applications/main.c \
../applications/mqtt.c \
../applications/sensors.c \
../applications/sound.c \
../applications/wifi.c 

OBJS += \
./applications/key.o \
./applications/led.o \
./applications/main.o \
./applications/mqtt.o \
./applications/sensors.o \
./applications/sound.o \
./applications/wifi.o 

C_DEPS += \
./applications/key.d \
./applications/led.d \
./applications/main.d \
./applications/mqtt.d \
./applications/sensors.d \
./applications/sound.d \
./applications/wifi.d 


# Each subdirectory must supply rules for building sources it contributes
applications/%.o: ../applications/%.c
	arm-none-eabi-gcc -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\N32_Std_Driver\CMSIS\core" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\N32_Std_Driver\CMSIS\device" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\N32_Std_Driver\n32g45x_std_periph_driver\inc" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\rt_drivers" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\applications" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\board\msp" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\board" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\at_device-latest\class\esp8266" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\at_device-latest\inc" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\cJSON-v1.0.2" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\pahomqtt-v1.1.0\MQTTClient-RT" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\pahomqtt-v1.1.0\MQTTPacket\src" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\dfs\filesystems\devfs" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\dfs\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\drivers\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\finsh" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\libc\compilers\common" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\libc\compilers\gcc\newlib" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\at\at_socket" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\at\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\netdev\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\impl" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include\dfs_net" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include\socket\sys_socket" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include\socket" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\libcpu\arm\common" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\libcpu\arm\cortex-m4" -include"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

