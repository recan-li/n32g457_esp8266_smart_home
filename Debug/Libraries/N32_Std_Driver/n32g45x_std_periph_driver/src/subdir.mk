################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/misc.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_adc.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_bkp.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_can.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_comp.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_crc.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dac.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dbg.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dma.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dvp.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_eth.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_exti.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_flash.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_gpio.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_i2c.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_iwdg.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_opamp.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_pwr.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_qspi.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rcc.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rtc.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_sdio.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_spi.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tim.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tsc.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_usart.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_wwdg.c \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_xfmc.c 

O_SRCS += \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/misc.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_adc.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_bkp.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_can.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_comp.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_crc.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dac.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dbg.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dma.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dvp.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_eth.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_exti.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_flash.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_gpio.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_i2c.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_iwdg.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_opamp.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_pwr.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_qspi.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rcc.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rtc.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_sdio.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_spi.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tim.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tsc.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_usart.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_wwdg.o \
../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_xfmc.o 

OBJS += \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/misc.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_adc.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_bkp.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_can.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_comp.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_crc.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dac.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dbg.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dma.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dvp.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_eth.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_exti.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_flash.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_gpio.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_i2c.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_iwdg.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_opamp.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_pwr.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_qspi.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rcc.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rtc.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_sdio.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_spi.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tim.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tsc.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_usart.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_wwdg.o \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_xfmc.o 

C_DEPS += \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/misc.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_adc.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_bkp.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_can.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_comp.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_crc.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dac.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dbg.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dma.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_dvp.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_eth.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_exti.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_flash.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_gpio.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_i2c.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_iwdg.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_opamp.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_pwr.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_qspi.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rcc.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_rtc.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_sdio.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_spi.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tim.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_tsc.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_usart.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_wwdg.d \
./Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/n32g45x_xfmc.d 


# Each subdirectory must supply rules for building sources it contributes
Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/%.o: ../Libraries/N32_Std_Driver/n32g45x_std_periph_driver/src/%.c
	arm-none-eabi-gcc -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\N32_Std_Driver\CMSIS\core" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\N32_Std_Driver\CMSIS\device" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\N32_Std_Driver\n32g45x_std_periph_driver\inc" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\Libraries\rt_drivers" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\applications" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\board\msp" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\board" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\at_device-latest\class\esp8266" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\at_device-latest\inc" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\cJSON-v1.0.2" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\pahomqtt-v1.1.0\MQTTClient-RT" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\packages\pahomqtt-v1.1.0\MQTTPacket\src" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\dfs\filesystems\devfs" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\dfs\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\drivers\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\finsh" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\libc\compilers\common" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\libc\compilers\gcc\newlib" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\at\at_socket" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\at\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\netdev\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\impl" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include\dfs_net" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include\socket\sys_socket" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include\socket" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\components\net\sal_socket\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\include" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\libcpu\arm\common" -I"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rt-thread\libcpu\arm\cortex-m4" -include"C:\RT-ThreadStudio\workspace\n32g457_esp8266_smart_home\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

