/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-24     takeout       the first version
 */

#include <string.h>
#include "rtthread.h"
#include "rtdef.h"
#include "rtdevice.h"
#include "sensors.h"
#include "mqtt.h"
#include "sound.h"
#include "config.h"

#define DBG_TAG "sensors"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

static sensor_object_t g_sensors_table[] =
{
    DEFINE_A_SENSOR(SMOKE),       //smoke
    DEFINE_A_SENSOR(ALCOHOL_GAS), //alcohol gas
    DEFINE_A_SENSOR(NATURAL_GAS), //natural gas
    DEFINE_A_SENSOR(AIR_QUALITY), //air quality
};

static int g_sensor_status;

static void sensors_detect_thread_entry(void *parameter)
{
    rt_uint32_t i;
    rt_uint32_t cnt = ARRAY_SIZE(g_sensors_table);
    rt_uint32_t status;
    rt_sem_t sem = (rt_sem_t)parameter;

    for (i = 0; i < cnt; i++)
    {
#if (SENSOR_TRIGGER_VALUE == PIN_LOW)
        rt_pin_mode(g_sensors_table[i].pin, PIN_MODE_INPUT_PULLUP);
#else
        rt_pin_mode(g_sensors_table[i].pin, PIN_MODE_INPUT_PULLDOWN);
#endif
    }

    g_sensor_status = 0;

    while (1)
    {
        for (i = 0; i < cnt; i++)
        {
            status = rt_pin_read(g_sensors_table[i].pin);
            //LOG_D("sensor [%-10s] @ pin %d -> %d ...", g_sensors_table[i].name, g_sensors_table[i].pin, status);
            if (status == SENSOR_TRIGGER_VALUE)
            {
                if (g_sensors_table[i].last_status == CFG_SENDOR_NO_ALARTED_VALUE)
                {
                    rt_sem_release(sem);
                    update_device_status(g_sensors_table[i].idx, CFG_SENDOR_ALARTED_VALUE);
                }
                alert_sound_play_req((rt_uint32_t)i);
                g_sensors_table[i].last_status = CFG_SENDOR_ALARTED_VALUE;
                g_sensor_status |= g_sensors_table[i].mask;
                LOG_D("sensor [%-10s] @ pin %d trigger ...", g_sensors_table[i].name, g_sensors_table[i].pin);
            }
            else
            {
                if (g_sensors_table[i].last_status == CFG_SENDOR_ALARTED_VALUE)
                {
                    update_device_status(g_sensors_table[i].idx, CFG_SENDOR_NO_ALARTED_VALUE);
                }
                g_sensors_table[i].last_status = CFG_SENDOR_NO_ALARTED_VALUE;
                g_sensor_status &= ~g_sensors_table[i].mask;
            }
        }

        if (g_sensor_status) {
            LOG_D("g_sensor_status: %x", g_sensor_status);
        }

        rt_thread_mdelay(CFG_SENDOR_DETECT_CYCLE_TIME * 1000);
    }
}

int sensors_init(void *arg)
{
    rt_thread_t thread;

    thread = rt_thread_create("sensor_thread", sensors_detect_thread_entry, arg, 1024, 10, 20);
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
        return RT_EOK;
    }
    else
    {
        LOG_E("create sensor thread failed");
        return (-RT_ERROR);
    }
}

int sensors_get_status(void)
{
    return g_sensor_status;
}

int sensors_clear_status(void)
{
    g_sensor_status = 0;
    return g_sensor_status;
}


