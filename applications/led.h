#ifndef _LED_H_
#define _LED_H_

int alert_led_init(void *arg);
int alert_led_disable(void);

#endif  /* led.h */
