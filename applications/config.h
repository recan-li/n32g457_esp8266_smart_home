/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-27     takeout       the first version
 */
#ifndef APPLICATIONS_CONFIG_H_
#define APPLICATIONS_CONFIG_H_

#include <rtthread.h>
#include "rtdevice.h"

/* Wi-Fi module configuration */
#define CFG_WIFI_MODULE_USED_UART       "uart2"
#define CFG_WIFI_MODULE_USED_UART_BAUD  115200 //modify Sconscript at the same time
#define CFG_UART2_BAUD                  CFG_WIFI_MODULE_USED_UART_BAUD

/* sound module configuration */
#define CFG_SOUND_MODULE_USED_UART      "uart3"
#define CFG_SOUND_MODULE_USED_UART_BAUD 9600  //modify Sconscript at the same time
#define CFG_UART3_BAUD                  CFG_SOUND_MODULE_USED_UART_BAUD

/* key module confifuration */
#define CFG_KEY_USED_PIN                22 //PA6
#define CFG_KEY_PRESSED                 PIN_LOW

/* led module configuration */
#define CFG_LED_USED_PIN                57 //PB5
#define CFG_LED2_USED_PIN               41 //PA8
#define CFG_LED3_USED_PIN               56 //PB4
#define CFG_LED_ON                      PIN_HIGH
#define CFG_LED_OFF                     PIN_LOW

/* sensor module configuration */
#define CFG_SMOKE_USED_PIN              37 //PC6
#define CFG_ALCOHOL_GAS_USED_PIN        39 //PC8
#define CFG_NATURAL_GAS_USED_PIN        41 //PA8
#define CFG_AIR_QUALITY_USED_PIN        43 //PB10

#define CFG_SMOKE_USED_INDEX            0
#define CFG_ALCOHOL_GAS_USED_INDEX      1
#define CFG_NATURAL_GAS_USED_INDEX      2
#define CFG_AIR_QUALITY_USED_INDEX      3

#define CFG_SENSOR_NO_TRIGGER_VALUE     PIN_HIGH
#define CFG_SENSOR_TRIGGER_VALUE        PIN_LOW

#define CFG_SENDOR_NO_ALARTED_VALUE     0x00
#define CFG_SENDOR_ALARTED_VALUE        0x01

#define CFG_SENDOR_DETECT_CYCLE_TIME    5 // seconds

/* Domoticz device idx configuration */
#define CFG_SMOKE_USED_IDX              1
#define CFG_ALCOHOL_GAS_USED_IDX        4
#define CFG_NATURAL_GAS_USED_IDX        3
#define CFG_AIR_QUALITY_USED_IDX        2
#define CFG_ALERT_LED_USED_IDX          5

#define CFG_ALERT_LED_OFF_VALUE         0x00
#define CFG_ALERT_LED_ON_VALUE          0x01

/* MQTT module configuration */
#define CFG_MQTT_URI                    "tcp://8.129.222.85:1883"
#define CFG_MQTT_DOMOTICZ_TOPIC_IN      "domoticz/in"
#define CFG_MQTT_DOMOTICZ_TOPIC_OUT     "domoticz/out"
#define CFG_MQTT_CLIENT_ID              "test-user"
#define CFG_MQTT_USERNAME               ""
#define CFG_MQTT_PASSWORD               ""

#endif /* APPLICATIONS_CONFIG_H_ */
