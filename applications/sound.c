/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-25     takeout       the first version
 */

#include <rtthread.h>
#include "rtdef.h"
#include "rtdevice.h"

#include "config.h"

#define DBG_TAG "sound"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

/*
 * 程序清单：这是一个 串口 设备使用例程
 * 例程导出了 uart_sample 命令到控制终端
 * 命令调用格式：uart_sample uart2
 * 命令解释：命令第二个参数是要使用的串口设备名称，为空则使用默认的串口设备
 * 程序功能：通过串口输出字符串"hello RT-Thread!"，然后错位输出输入的字符
*/

#include <rtthread.h>

#define SAMPLE_UART_NAME       CFG_SOUND_MODULE_USED_UART

/* 用于接收消息的信号量 */
static struct rt_semaphore rx_sem;
static rt_device_t serial;

/* 接收数据回调函数 */
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    /* 串口接收到数据后产生中断，调用此回调函数，然后发送接收信号量 */
    rt_sem_release(&rx_sem);

    return RT_EOK;
}

static void serial_thread_entry(void *parameter)
{
    char ch;

    while (1)
    {
        /* 从串口读取一个字节的数据，没有读取到则等待接收信号量 */
        while (rt_device_read(serial, -1, &ch, 1) != 1)
        {
            /* 阻塞等待接收信号量，等到信号量后再次读取数据 */
            rt_sem_take(&rx_sem, RT_WAITING_FOREVER);
        }
        /* 读取到的数据通过串口错位输出 */
        ch = ch + 1;
        rt_device_write(serial, 0, &ch, 1);
    }
}

static int alert_sound_uart_init(void)
{
    rt_err_t ret = RT_EOK;
    char uart_name[RT_NAME_MAX];

    rt_strncpy(uart_name, SAMPLE_UART_NAME, RT_NAME_MAX);

    /* 查找系统中的串口设备 */
    serial = rt_device_find(uart_name);
    if (!serial)
    {
        rt_kprintf("find %s failed!\n", uart_name);
        return RT_ERROR;
    }

    /* 初始化信号量 */
    rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
    /* 以中断接收及轮询发送模式打开串口设备 */
    rt_device_open(serial, RT_DEVICE_FLAG_INT_RX);
    /* 设置接收回调函数 */
    rt_device_set_rx_indicate(serial, uart_input);
    /* 发送字符串 */
    //rt_device_write(serial, 0, str, (sizeof(str) - 1));

    /* 创建 serial 线程 */
    rt_thread_t thread = rt_thread_create("serial", serial_thread_entry, RT_NULL, 1024, 25, 10);
    /* 创建成功则启动线程 */
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
    }
    else
    {
        ret = RT_ERROR;
    }

    return ret;
}

#define SOUND_PLAY_DATA_LEN 6

static rt_uint8_t g_is_frist_send = 1;

static rt_uint8_t g_sound_keeping = 1;

static rt_uint8_t g_sound_play_data[4][SOUND_PLAY_DATA_LEN] =
{
    {0xAA, 0x07, 0x02, 0x00, 0x05, 0xB8,}, //烟雾探测器 报警声音
    {0xAA, 0x07, 0x02, 0x00, 0x08, 0xBB,}, //酒精探测器 报警声音
    {0xAA, 0x07, 0x02, 0x00, 0x07, 0xBA,}, //天然气探测器 报警声音
    {0xAA, 0x07, 0x02, 0x00, 0x06, 0xB9,}, //空气质量探测器 报警声音
};

/* 邮箱控制块 */
static struct rt_mailbox mb;
/* 用于放邮件的内存池 */
static char mb_pool[128];

static int alert_sound_play(rt_uint32_t index)
{
    rt_uint8_t *data = g_sound_play_data[index];

    if (!g_is_frist_send)
    {
        LOG_D("---%02x %02x", data[0], data[1]);
        rt_device_write(serial, 0, data, SOUND_PLAY_DATA_LEN);
    }
    else
    {
        rt_uint8_t new_data[SOUND_PLAY_DATA_LEN + 1];

        g_is_frist_send = 0;
        new_data[0] = 0xAA; //add 1 byte
        rt_memcpy(&new_data[1], data, SOUND_PLAY_DATA_LEN);
        LOG_D("+++%02x %02x", new_data[0], new_data[1]);
        rt_device_write(serial, 0, new_data, sizeof(new_data));
    }

    return RT_EOK;
}

int alert_sound_play_req(rt_uint32_t index)
{
    rt_mb_send(&mb, (rt_ubase_t)index);
    return RT_EOK;
}

static int alert_sound_mail_init(void)
{
    rt_err_t result;

    /* 初始化一个 mailbox */
    result = rt_mb_init(&mb,
                        "mbt",                      /* 名称是 mbt */
                        &mb_pool[0],                /* 邮箱用到的内存池是 mb_pool */
                        sizeof(mb_pool) / 4,        /* 邮箱中的邮件数目，因为一封邮件占 4 字节 */
                        RT_IPC_FLAG_FIFO);          /* 采用 FIFO 方式进行线程等待 */
    if (result != RT_EOK)
    {
        rt_kprintf("init mailbox failed.\n");
        return RT_ERROR;
    }

    return RT_EOK;
}

int alert_sound_disable(void)
{
    g_sound_keeping = 0;
    return RT_EOK;
}

int alert_sound_enable(void)
{
    g_sound_keeping = 1;
    return RT_EOK;
}

static void alert_sound_thread_entry(void *parameter)
{
    rt_ubase_t index;

    while (1)
    {
        if (rt_mb_recv(&mb, (rt_ubase_t *)&index, RT_WAITING_FOREVER) == RT_EOK)
        {
            LOG_D("index: %d", index);
            if (g_sound_keeping)
            {
                alert_sound_play(index);
                rt_thread_mdelay(6000); //wait play over
            }
        }
    }
}

int alert_sound_init(void *arg)
{
    alert_sound_uart_init();
    alert_sound_mail_init();

    rt_thread_t thread = rt_thread_create("sound_thread", alert_sound_thread_entry, arg, 512, 10, 20);
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
        return RT_EOK;
    }
    else
    {
        LOG_E("create led3 thread failed");
        return (-RT_ERROR);
    }

    return RT_EOK;
}


