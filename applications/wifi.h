#ifndef _N32_ESP8266_H_
#define _N32_ESP8266_H_

int wifi_init(void *arg);
int wifi_get_status(void);

#endif  /* n32_esp8266.h */
