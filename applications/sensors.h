/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-24     takeout       the first version
 */
#ifndef APPLICATIONS_SENSORS_H_
#define APPLICATIONS_SENSORS_H_

#include "config.h"

/* sensor trigger value */
#define SENSOR_NO_TRIGGER_VALUE CFG_SENSOR_NO_TRIGGER_VALUE
#define SENSOR_TRIGGER_VALUE    CFG_SENSOR_TRIGGER_VALUE

/* pin config */
/* domoticz idx config */
#define SMOKE_PIN               CFG_SMOKE_USED_PIN
#define ALCOHOL_GAS_PIN         CFG_ALCOHOL_GAS_USED_PIN
#define NATURAL_GAS_PIN         CFG_NATURAL_GAS_USED_PIN
#define AIR_QUALITY_PIN         CFG_AIR_QUALITY_USED_PIN

/* mask config */
#define SMOKE_MASK              (1 << CFG_SMOKE_USED_INDEX)
#define ALCOHOL_GAS_MASK        (1 << CFG_ALCOHOL_GAS_USED_INDEX)
#define NATURAL_GAS_MASK        (1 << CFG_NATURAL_GAS_USED_INDEX)
#define AIR_QUALITY_MASK        (1 << CFG_AIR_QUALITY_USED_INDEX)

/* domoticz idx config */
#define SMOKE_IDX               CFG_SMOKE_USED_IDX
#define ALCOHOL_GAS_IDX         CFG_ALCOHOL_GAS_USED_IDX
#define NATURAL_GAS_IDX         CFG_NATURAL_GAS_USED_IDX
#define AIR_QUALITY_IDX         CFG_AIR_QUALITY_USED_IDX

#define DEFINE_A_SENSOR(name) \
    {\
        (#name),\
        (name ## _PIN),\
        (name ## _MASK),\
        (name ## _IDX),\
        (0),\
    }

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)   (sizeof(a) / sizeof(a[0]))
#endif

typedef struct _sensor_object_t {
    const char *name;
    rt_base_t   pin;
    rt_base_t   mask;
    rt_base_t   idx;
    rt_base_t   last_status;
} sensor_object_t;

int sensors_init(void *arg);
int sensors_get_status(void);
int sensors_clear_status(void);

#endif /* APPLICATIONS_SENSORS_H_ */
