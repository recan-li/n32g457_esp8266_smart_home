#ifndef _MQTT_H_
#define _MQTT_H_

int mqtt_init(void *arg);
int update_device_status(int idx, int nvalue);

#endif  /* mqtt.h */
