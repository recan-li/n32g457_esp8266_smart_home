/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-25     takeout       the first version
 */
#ifndef APPLICATIONS_SOUND_H_
#define APPLICATIONS_SOUND_H_

int alert_sound_init(void *arg);
int alert_sound_enable(void);
int alert_sound_disable(void);
int alert_sound_play_req(rt_uint32_t index);

#endif /* APPLICATIONS_SOUND_H_ */
