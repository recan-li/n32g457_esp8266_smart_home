#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <rtthread.h>
#include "rtdevice.h"

#include "config.h"
#include "led.h"
#include "sound.h"

#define DBG_TAG "key.c"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#define ALERT_CLR_KEY  CFG_KEY_USED_PIN

/**
 * @brief  KEY 线程执行函数
 * @param  parameter 线程的入参
 * @return 无
 */
void key_thread_entry(void *parameter)
{
    while (1)
    {
        if(rt_pin_read(ALERT_CLR_KEY) == CFG_KEY_PRESSED)   // 如果按键3按下
        {
            rt_thread_mdelay(50);   // 延时消抖
            if(rt_pin_read(ALERT_CLR_KEY) == CFG_KEY_PRESSED)
            {
                alert_led_disable();
                alert_sound_disable();
            }
        }
        else
        {
            rt_thread_mdelay(100);
        }
    }
}

/**
 * @brief  启动 KEY 处理线程
 * @param  无
 * @return 无
 */
int key_init(void *arg)
{
    rt_pin_mode(ALERT_CLR_KEY, PIN_MODE_INPUT_PULLUP);

    rt_thread_t thread = rt_thread_create("key_thread", key_thread_entry, RT_NULL, 256, 10, 20);
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);  // 创建成功则启动线程
    }
    else
    {
        LOG_E("create key thread failed");
        return (-RT_ERROR);
    }

    return RT_EOK;
}
